﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkhoundAnimationChooser : MonoBehaviour {

	private Animator anim;
	private InputReader ir;

	// Use this for initialization
	void Awake () {
		anim = GetComponent<Animator>();
		ir = GetComponent<InputReader>();
	}
	
	// Update is called once per frame
	void Update () {
		anim.SetFloat("x", ir.getRawX());
		anim.SetFloat("lastX", ir.getLastRawX());
		if (ir.getRawX() != 0) {
			anim.SetBool("Moving", true);
		}
		else {
			anim.SetBool("Moving", false);
		}
	}
}
