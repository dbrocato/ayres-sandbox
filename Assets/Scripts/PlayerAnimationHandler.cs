﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerEntity))]
public class PlayerAnimationHandler : MonoBehaviour {

	private Animator anim;
	private float x;
	private float lastX;
	private GroundedChecker gc;
	private PlayerEntity entity;

	// Use this for initialization
	void Awake () {
		anim = GetComponent<Animator>();
		gc = GetComponent<GroundedChecker>();
		entity = GetComponent<PlayerEntity>();

		anim.SetFloat("X", 1);
		anim.SetFloat("lastX", 1);
	}
	
	// Update is called once per frame

	// TODO: Implement use of InputReader class.
	void Update () {
		
		x = Input.GetAxisRaw("Horizontal");
		anim.SetFloat("X", x);

		if (gc.isGrounded() || entity.basicAttack.isAttacking()) {
			anim.SetBool("Jump", false);
		}
		else {
			anim.SetBool("Jump", true);
		}

		//if (Input.GetMouseButtonDown(0) && entity.basicAttack.IsOffCooldown()) {
		//		Debug.Log("Set");
		//		anim.SetTrigger("BasicAttack");
		//} THIS IS NOT WORKING. SEE BasicAttackCommand CLASS.

		if (x != 0) {
			if (Input.GetKey(KeyCode.LeftShift)) {
				anim.SetBool("Walk", false);
				anim.SetBool("Run", true);
			}
			else {
				anim.SetBool("Walk", true);
				anim.SetBool("Run", false);
			}

			lastX = x;
			anim.SetFloat("lastX", lastX);
		}
		else {
			anim.SetBool("Walk", false);
			anim.SetBool("Run", false);
		}
	}
}
