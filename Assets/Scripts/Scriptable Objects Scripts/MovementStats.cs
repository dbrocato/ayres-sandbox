﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Scriptable object to store data about the entity's movement stats, such as max walk/sprint speeds, acceleration forces, and jump force.
*/
[CreateAssetMenu (menuName = "Movement Stats")]
public class MovementStats : ScriptableObject {
	public float walkForce;
	public float walkSpeedCap;
	public float runForce;
	public float runSpeedCap;
	public float jumpForce;
	public float airCorrectionForce;
}
