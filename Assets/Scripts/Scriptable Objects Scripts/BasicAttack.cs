﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Basic Attack")]
public class BasicAttack : ScriptableObject {
	public float cooldown; // Cooldown on the attack.
	public GameObject hitboxCollider; // Better if this is a prefab. See BasicAttackCommand constructor.
	public float windUpTime; // Duration before the attack is actually cast, i.e. animation has a wind up before attack hitbox should be active. Set to 0 if attack is instant.
	public float duration; // Duration that the hitbox will remain active once the attack is cast.

	// Note: Lunge force is generally quite high, as it is only applied once before it is halted by base friction and lunge deceleration.
	public float lungeForce; // Force with which the entity lunges forward when using this attack. The velocity during the lunge is not capped by MoveCommand.

	public float lungeDeceleration; // Force applied after the lunge to slow down. Could be considered drag or friction.
}
