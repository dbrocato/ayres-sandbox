﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Player Form")]
public class PlayerForm : ScriptableObject {

	public MovementStats movementStats;
	public BasicAttack basicAttack;
	public AnimatorOverrideController animationController;
	
}
