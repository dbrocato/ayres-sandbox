﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAttackCommand : Command {

	private Rigidbody2D rb;
	private Animator anim;
	private bool attacking;
	private GameObject leftCollider;
	private GameObject rightCollider;
	private Entity entity;
	private float attackDuration;
	private float lungeForce;
	private float windUpTime;

	private BasicAttack attack;

	private InputReader inputReader;

	private float cooldownStart = 0;

	/*
	BasicAttack object includes data for a basic attack, such as cooldown and hitboxes.
	*/
	public BasicAttackCommand(Entity entity, BasicAttack basicAttack) : base(entity) {

		attack = basicAttack;
		this.entity = entity;
		
		SetColliders();
		
		rb = entity.GetComponent<Rigidbody2D>(); // Rigidbody for adding force.
		anim = entity.GetComponent<Animator>();
		inputReader = entity.GetComponent<InputReader>();
	}

	public override void Execute() {
		if (IsOffCooldown()) {
			float direction = inputReader.getLastRawX();
			anim.SetTrigger("BasicAttack"); // Setting animation here because it is not working in AnimationChooser when IsOffCooldown() is public.
			cooldownStart = Time.time;
			entity.StartCoroutine(Attack(direction)); // Starts an Attack coroutine on the entity receiving this command. Direction obtained from input reader. last X to work if standing still.
		}
		else {
			Debug.Log("Basic Attack is on cooldown");
		}
	}

	/*
	@param dir The direction the Actor is facing when this coroutine begins. If 0, right is default (this shouldn't ever really happen).
	*/
	IEnumerator Attack(float dir) {
		attacking = true; // Used for limiting the commands that can occur during an attack command.

		yield return new WaitForSeconds(attack.windUpTime); // Waits for the wind up animation before collider activation and lunge.

		rb.AddForce(Vector2.right * attack.lungeForce * dir); // Lunges after the wind up.

		SetColliderActive(dir, true); // Activates the collider with the appropriate direction

		yield return new WaitForSeconds(attack.duration); // Keeps colliders active for whole attack.

		SetColliderActive(dir, false); // Deactivates the collider with the appropriate direction

		attacking = false; // Used for limiting the commands that can occur during an attack command.
	}

	public bool isAttacking() {
		return attacking; // Used for limiting the commands that can occur during an attack command.
	}

	bool IsOffCooldown() {
		if (cooldownStart + attack.cooldown <= Time.time) {
			return true;
		}
		else {
			return false;
		}
	}

	void SetColliders() { // Creates the colliders to use for this attack and initially deactivates them.
		rightCollider = GameObject.Instantiate(attack.hitboxCollider, entity.transform); // Creates collider from prefab and receives entity as parent.
		leftCollider = GameObject.Instantiate(attack.hitboxCollider, entity.transform); // Creates collider from prefab and receives entity as parent.
		leftCollider.transform.Rotate(0, 180, 0); // Mirrors the right-faced collider for use on the left side.
		leftCollider.SetActive(false);
		rightCollider.SetActive(false);
	}

	void SetColliderActive(float dir, bool active) { // Activates and deactivates the colliders of the appropriate directions.
		if (active) {
 			if (dir >= 0) {
				rightCollider.SetActive(true);
			}
			else {
				leftCollider.SetActive(true);
			}
		}
		else {
			if (dir >= 0) {
				rightCollider.SetActive(false);
			}
			else {
				leftCollider.SetActive(false);
			}
		}
	} 
}
