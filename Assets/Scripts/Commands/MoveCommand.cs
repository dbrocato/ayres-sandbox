﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Used for walking and running.
public class MoveCommand : Command {

	private Vector2 rbvel;
	private float force;
	private float speedCap;
	private Rigidbody2D rb;

	public MoveCommand(Entity entity, float accelerationForce, float maxSpeed) : base(entity) {
		rbvel = new Vector2(0, 0);
		speedCap = maxSpeed;
		force = accelerationForce;
		rb = entity.GetComponent<Rigidbody2D>();
	}

	public override void Execute() {
		float direction = Input.GetAxisRaw("Horizontal");

		rb.AddForce(Vector2.right * force * direction);
			
		if (rb.velocity.x > speedCap) { // Limits the speed for pos x.
			rbvel.Set(speedCap, rb.velocity.y);
			rb.velocity = rbvel;
		}
		if (rb.velocity.x < -speedCap) { // Limits the speed for neg x.
			rbvel.Set(-speedCap, rb.velocity.y);
			rb.velocity = rbvel;
		}
	}
}
