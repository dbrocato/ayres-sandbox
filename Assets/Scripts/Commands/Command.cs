﻿public abstract class Command {

	protected Entity _entity;

	public Command(Entity entity) { // Entity to execute command on. Allows access to components of entity as well.
		_entity = entity;
	}

	public abstract void Execute();
}