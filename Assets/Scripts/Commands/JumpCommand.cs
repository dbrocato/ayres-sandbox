﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpCommand : Command {

	private Rigidbody2D rb;
	private float force;
	private float initialJumpSpeed;

	public JumpCommand(Entity entity, float jumpForce) : base(entity) {
		rb = entity.GetComponent<Rigidbody2D>();
		force = jumpForce;
	}

	public override void Execute() {
		initialJumpSpeed = Mathf.Abs(rb.velocity.x);
		rb.AddForce(Vector2.up * force, ForceMode2D.Impulse);
	}

	public float getInitialJumpSpeed() {
		return initialJumpSpeed;
	}
}
