﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirMoveCommand : Command {

	private Rigidbody2D rb;
	private Vector2 rbvel;
	private float force;
	private float initialAirSpeed;

	public AirMoveCommand(Entity entity, float airCorrectionForce) : base(entity) {
		rbvel = new Vector2(0, 0);
		rb = entity.GetComponent<Rigidbody2D>();
		force = airCorrectionForce;
	}

	public override void Execute() {
		float direction = Input.GetAxis("Horizontal");

		rb.AddForce(Vector2.right * direction * force);

		if (rb.velocity.x > initialAirSpeed) { // Limits the speed for pos x.
			rbvel.Set(initialAirSpeed, rb.velocity.y);
			rb.velocity = rbvel;
		}
		if (rb.velocity.x < -initialAirSpeed) { // Limits the speed for neg x.
			rbvel.Set(-initialAirSpeed, rb.velocity.y);
			rb.velocity = rbvel;
		}
	}

	public void setInitialAirSpeed(float speed) {
		initialAirSpeed = speed;
	}
}
