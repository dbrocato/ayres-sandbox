﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlternateMoveCommand : Command {

	private Rigidbody2D rb;
	private float speed;
	private Vector2 vec2;

	public AlternateMoveCommand(Entity entity, float moveSpeed) : base(entity) {
		rb = entity.GetComponent<Rigidbody2D>();
		speed = moveSpeed;
		vec2 = new Vector2(0, 0);
	}

	public override void Execute() {
		vec2.Set(rb.position.x + Input.GetAxis("Horizontal") * speed * Time.deltaTime, rb.position.y);
		rb.MovePosition(vec2);
	}
}
