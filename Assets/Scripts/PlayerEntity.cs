﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEntity : Entity {

	private GroundedChecker gc;
	private BoxCollider2D boxCollider;
	private Rigidbody2D rb;

	public Animator anim;

	private InputReader inputReader;

	private float deceleration = 800; // Deceleration force when halting. Stands in place of friction.
	private float attackDeceleration = 200; // Deceleration force after the lunge force of an attack.

	public PlayerForm ayres;
	public PlayerForm rhan;
	public PlayerForm inao;
	public PlayerForm yuna;

	public MoveCommand walk;
	public MoveCommand run;
	public JumpCommand jump;
	public AirMoveCommand airMove;
	public BasicAttackCommand basicAttack;

	void Awake () {
		rb = GetComponent<Rigidbody2D>();
		gc = GetComponent<GroundedChecker>();
		inputReader = GetComponent<InputReader>();

		anim = GetComponent<Animator>();

		setAyresForm();
	}

	void Update () {

		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			print ("Ayres");
			setAyresForm();
		}
		else if (Input.GetKeyDown(KeyCode.Alpha2)) {
			print ("Rhan");
			setRhanForm();
		}
		else if (Input.GetKeyDown(KeyCode.Alpha3)) {
			print ("Inao");
			setInaoForm();
		}
		else if (Input.GetKeyDown(KeyCode.Alpha4)) {
			print ("Yuna");
			setYunaForm();
		}

		if (Input.GetKeyDown(KeyCode.Space) && gc.isGrounded()) {
			jump.Execute();
		}

		if (Input.GetMouseButtonDown(0)) {
			basicAttack.Execute();
		}
	}

	void FixedUpdate () {
		// Halts without sliding. If feel is strange, try moving back to Update().
		if (inputReader.getRawX() == 0 && gc.isGrounded() && !basicAttack.isAttacking()) { // Ensures that entity is not attacking.
			rb.AddForce(Vector2.right * -rb.velocity.x * deceleration);
		}

		if (basicAttack.isAttacking() && gc.isGrounded()) {
			rb.AddForce(Vector2.right * -rb.velocity.x * attackDeceleration);
		}

		if (!basicAttack.isAttacking()) { // Ensures that entity is not attacking.
			if (inputReader.getRawX() != 0) {
				if (gc.isGrounded()) {
					if (Input.GetKey(KeyCode.LeftShift)) {
						run.Execute();
					}
					else {
						walk.Execute();
					}
				}
				else {
					airMove.setInitialAirSpeed(jump.getInitialJumpSpeed());
					airMove.Execute();
				}
			}
		}
	}

	void setAyresForm() {
		walk = new MoveCommand(this, ayres.movementStats.walkForce, ayres.movementStats.walkSpeedCap);
		run = new MoveCommand(this, ayres.movementStats.runForce, ayres.movementStats.runSpeedCap);
		jump = new JumpCommand(this, ayres.movementStats.jumpForce);
		airMove = new AirMoveCommand(this, ayres.movementStats.airCorrectionForce);
		basicAttack = new BasicAttackCommand(this, ayres.basicAttack);
		anim.runtimeAnimatorController = ayres.animationController;
	}

	void setRhanForm() {
		walk = new MoveCommand(this, rhan.movementStats.walkForce, rhan.movementStats.walkSpeedCap);
		run = new MoveCommand(this, rhan.movementStats.runForce, rhan.movementStats.runSpeedCap);
		jump = new JumpCommand(this, rhan.movementStats.jumpForce);
		airMove = new AirMoveCommand(this, rhan.movementStats.airCorrectionForce);
		basicAttack = new BasicAttackCommand(this, rhan.basicAttack);
		anim.runtimeAnimatorController = rhan.animationController;
	}

	void setInaoForm() {
		walk = new MoveCommand(this, inao.movementStats.walkForce, inao.movementStats.walkSpeedCap);
		run = new MoveCommand(this, inao.movementStats.runForce, inao.movementStats.runSpeedCap);
		jump = new JumpCommand(this, inao.movementStats.jumpForce);
		airMove = new AirMoveCommand(this, inao.movementStats.airCorrectionForce);
		basicAttack = new BasicAttackCommand(this, inao.basicAttack);
		anim.runtimeAnimatorController = inao.animationController;
	}

	void setYunaForm() {
		walk = new MoveCommand(this, yuna.movementStats.walkForce, yuna.movementStats.walkSpeedCap);
		run = new MoveCommand(this, yuna.movementStats.runForce, yuna.movementStats.runSpeedCap);
		jump = new JumpCommand(this, yuna.movementStats.jumpForce);
		airMove = new AirMoveCommand(this, yuna.movementStats.airCorrectionForce);
		basicAttack = new BasicAttackCommand(this, yuna.basicAttack);
		anim.runtimeAnimatorController = yuna.animationController;
	}
}
