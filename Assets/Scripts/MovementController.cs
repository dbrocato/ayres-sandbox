﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {

	public float accelerationForce;
	public float maxSpeed;
	public float sprintMultiplier;
	public float maxSprintSpeed;
	public float jumpForce;
	public float airCorrectionForce;

	private GroundedChecker gc;
	private float initialJumpSpeed;
	private Rigidbody2D rb;

	/*
	rbvel exists as a placeholder for when it is necessary to modify the
	velocity directly (via Vector2 assignment). This saves runtime memory,
	as this Vector2 is being made only once and values are set to it instead
	of creating a new Vector2 every time it is necessary to change velocity.
	It also allows for the saving of the y velocity component to keep it
	unchanged when only adjusting the x component, and vice-versa.
	*/
	private Vector2 rbvel;

	private float direction;

	// Use this for initialization
	void Awake () {
		rb = GetComponent<Rigidbody2D>();
		gc = GetComponent<GroundedChecker>();
		direction = Input.GetAxisRaw("Horizontal");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space) && gc.isGrounded()) {
			initialJumpSpeed = Mathf.Abs(rb.velocity.x);
			rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
		}
	}

	void FixedUpdate () {

		direction = Input.GetAxisRaw("Horizontal");

		/*
		This movement implementation is more "slide" based than the hard-stop
		you get with changing the velocity directly. This slide-based implementation
		feels more realistic. TODO: Implement "slide recoil" animation to occur when
		changing directions.
		*/
		if (gc.isGrounded()) { // Walk and sprint controls if on the ground.
			GroundMovement();
		}
		else { // Air adjustment control if off the ground.
			AirMovement();
		}
	}

	void HardStopMovement() {
		if (direction != Input.GetAxisRaw("Horizontal") && gc.isGrounded()) {
			rbvel.Set(0, rb.velocity.y);
			rb.velocity = rbvel;
		}

		direction = Input.GetAxisRaw("Horizontal");

		if (gc.isGrounded()) {
			if (Input.GetKey(KeyCode.LeftShift)) {
				rb.AddForce(Vector2.right * accelerationForce * sprintMultiplier * direction);
				if (Mathf.Abs(rb.velocity.x) > maxSprintSpeed) {
					rbvel.Set(maxSprintSpeed * direction, rb.velocity.y);
					rb.velocity = rbvel;
				}
			}
			else {
				rb.AddForce(Vector2.right * accelerationForce * direction);
				
				if (Mathf.Abs(rb.velocity.x) > maxSpeed) {
					rbvel.Set(maxSpeed * direction, rb.velocity.y);
					rb.velocity = rbvel;
				}
			}
		}
	}

	void GroundMovement() {
		if (Input.GetKey(KeyCode.LeftShift)) {
			rb.AddForce(Vector2.right * accelerationForce * sprintMultiplier * direction);
			if (rb.velocity.x > maxSprintSpeed) { // Limits the speed for pos x.
				rbvel.Set(maxSprintSpeed, rb.velocity.y);
				rb.velocity = rbvel;
			}
			if (rb.velocity.x < -maxSprintSpeed) { // Limits the speed for neg x.
				rbvel.Set(-maxSprintSpeed, rb.velocity.y);
				rb.velocity = rbvel;
			}
		}
		else {
			rb.AddForce(Vector2.right * accelerationForce * direction);
			
			if (rb.velocity.x > maxSpeed) { // Limits the speed for pos x.
				rbvel.Set(maxSpeed, rb.velocity.y);
				rb.velocity = rbvel;
			}
			if (rb.velocity.x < -maxSpeed) { // Limits the speed for neg x.
				rbvel.Set(-maxSpeed, rb.velocity.y);
				rb.velocity = rbvel;
			}
		}
	}

	void AirMovement() {
		rb.AddForce(Vector2.right * direction * airCorrectionForce);
		if (rb.velocity.x > initialJumpSpeed) { // Limits the speed for pos x.
			rbvel.Set(initialJumpSpeed, rb.velocity.y);
			rb.velocity = rbvel;
		}
		if (rb.velocity.x < -initialJumpSpeed) { // Limits the speed for neg x.
			rbvel.Set(-initialJumpSpeed, rb.velocity.y);
			rb.velocity = rbvel;
		}
	}
}
