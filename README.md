Platformer project in Unity. All assets were made by myself (Damien Brocato).

This project began as a game idea but eventually became an environment where I could test out any 2D platformer mechanic that I could think of. Spending numerous hours on these assets and scripts has allowed me to practice proper coding practices, project management, engineering, and using programming structures and paradigms. There isn't much to play, per se. Instead, my code is what I wish to display here.

See C# scripts for more info. Most are decently commented.